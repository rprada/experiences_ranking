class CreateEngineers < ActiveRecord::Migration[5.2]
  def change
    create_table :engineers do |t|

      t.string     :name  
      t.string     :do_not_try_this
      t.integer    :experience_days

      t.timestamps
    end
  end
end
