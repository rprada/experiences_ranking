class CreateEngineersJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :engineers_jobs do |t|

      t.references :engineer, index: true, foreign_key: true
      t.string   :employer
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
