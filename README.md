# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 2.6.1

* System dependencies

* Creation & Initialization
    
    - rails db:create

    - rails db:migrate

    - bundle install      

* Services (job queues)
    
    - rake job:get_data
    
    - rake job:experience_days
    
    - rake job:export_csv
    
* Command for show logs of jobs 
    
    - sidekiq -C config/sidekiq.yml


NOTE: 
   
   - The CSV file will save in the Public folder

   - All commands must be run inside the project path

* ...