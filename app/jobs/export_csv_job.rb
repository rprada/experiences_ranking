class ExportCsvJob < ApplicationJob
  queue_as :default

  require 'csv'
  require 'fileutils'
  
  def perform(*args)
    # Do something later
    do_work
  end

  def do_work
    #Caperta raiz donde se guardara el archivo
    folder_path = Rails.root.join('public')
    csv_path    = File.join(folder_path, 'ranking2018.csv')

    file = CSV.open(csv_path, 'wb') do |csv|
        csv << %w(ranking)
        csv << [ generate_list ]
    end
    puts "CSV File generated successfully :) !"
  end

  def generate_list
    list  = {}
    array = []

    engineers = Engineer.order(experience_days: "DESC")
    engineers.map do |engineer|
       key = { name: engineer.name, work_experience_days: engineer.experience_days }
       array << key
    end
    list['engineers'] = array
    list.to_json
  end
end
