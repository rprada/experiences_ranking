class GetApiDataJob < ApplicationJob
  queue_as :default

  def perform(*args)
    #reschedule_job
    do_work
  end

  def do_work
    require 'rest-client'
    data = JSON.parse( RestClient.get("https://demo0644754.mockable.io/engineers") )
    count = 1
    ActiveRecord::Base.transaction do
      puts "Connect #{count}"
      data['engineers'].each do |engineer|
        count += 1

        existing = Engineer.find_by(do_not_try_this: engineer["do_not_try_this"])
        unless existing
          @engineer = Engineer.create!(engineer)
            
            puts "Connect #{count}"
            data_jobs = JSON.parse( RestClient.get("https://demo0644754.mockable.io#{@engineer.do_not_try_this}") )
            if data_jobs['jobs'].is_a?(Array)
              data_jobs['jobs'].collect { |attr| @engineer.engineers_jobs.create!(attr) }
            end

            if count >= 4 
              puts "waiting 60 seconds for next connection"
              count = 0
              sleep(10) ##Waiting 1 min for avoid be blacklist
            end
        else
          puts "Engineer with ID #{engineer['id']} already exists"
        end
      end
      puts "Done!"
    end
  end

  def reschedule_job
    self.class.set(wait: 2.second).perform_later
  end
end
