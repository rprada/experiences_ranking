class CalculateRankingJob < ApplicationJob
  queue_as :default

    def perform(*args)
      # Do something later
      do_work
    end

    def do_work
        @engineers = Engineer.all
        ActiveRecord::Base.transaction do
          @engineers.any? do |engineer|
            hashe     = []
            hashe_end = []
            engineer.engineers_jobs.each_with_index do |job, idx| 
                # #################################
                # Fecha de término hasta 2018-12-31
                # #################################
                if ((job.start_date.nil?) || (job.start_date.to_date.year == 2019))
                    start_date   =  "2018-12-31".to_date
                else
                    start_date =  job.start_date.to_date
                end
                if ((job.end_date.nil?) || (job.end_date.to_date.year == 2019))
                    end_date   =  "2018-12-31".to_date
                else
                    end_date   =  job.end_date.to_date
                end
                # #################################
                # Validación traslape entre empleos 
                # #################################
                hashe << [start_date, end_date ]
                hashe.each_with_index do |attr, index|
                    unless idx == 0
                      unless attr == hashe.last
                        if (hashe[index][0]..hashe[index][1]).overlaps?(start_date..end_date)
                            hashe_end.delete([start_date, end_date])
                            break
                        else
                            hashe_end << [start_date, end_date ] unless hashe_end.include? [start_date, end_date ]
                        end
                      end
                    else
                        hashe_end << [start_date, end_date ] unless hashe_end.include? [start_date, end_date ]
                    end
                end
            end
            puts "#{engineer.id}.- #{engineer.name} -----> #{calculate_days(hashe_end)} days "
            engineer.update(experience_days: calculate_days(hashe_end) )
            next
          end
        end
    end

    # #################################
    # Cantidad de días entre fechas
    # #################################
    def calculate_days(dates)
        sum = 0
        dates.each do |date|
          days_period = (date[1] - date[0]).to_i
          sum = sum + days_period
        end
        total = sum + 1 # Adding day of EndDate
    end
end