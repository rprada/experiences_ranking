class Engineer < ApplicationRecord
    has_many :engineers_jobs, dependent: :destroy
    validates :name, :do_not_try_this, presence: { message: "can not be blank"}

end
