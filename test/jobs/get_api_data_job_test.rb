require 'test_helper'

class GetApiDataJobTest < ActiveJob::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @response = GetApiDataJob.perform_later "'testing Sidekiq queue jobs'"
  end

  test "enqueued jobs" do
    assert_enqueued_jobs 1
    clear_enqueued_jobs
    assert_enqueued_jobs 0
  end

  test "ActiveJob::QueueAdapters::SidekiqAdapter::JobWrapper" do
    assert_equal ["'testing Sidekiq queue jobs'"], @response.arguments
  end

  test "a second new job has been enqueued with the given arguments" do
    assert_enqueued_jobs 1
    assert_enqueued_with(job: GetApiDataJob, args: ["'queuing a second job'"], queue: 'default') do
      GetApiDataJob.perform_later "'queuing a second job'"
    end
    assert_enqueued_jobs 2
  end

end
