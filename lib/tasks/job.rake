namespace :job do
  desc "Get Data from API"
  task get_data: :environment do
    GetApiDataJob.perform_later
  end

  desc "Calculate experience days"
  task experience_days: :environment do
    CalculateRankingJob.perform_later
  end

  desc "Generate CSV file"
  task export_csv: :environment do
    ExportCsvJob.perform_later
  end
end
